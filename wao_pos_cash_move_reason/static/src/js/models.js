/** @odoo-module **/


    import {PosGlobalState} from 'point_of_sale.models';
    import Registries from 'point_of_sale.Registries';

    const CashReasonPosGlobalState = (PosGlobalState) => class CashReasonPosGlobalState extends PosGlobalState {
        async _processData(loadedData) {
            await super._processData(...arguments);
                this.cash_move_reasons = loadedData['pos.move.reason'];
                }
        }

    Registries.Model.extend(PosGlobalState, CashReasonPosGlobalState);
    
