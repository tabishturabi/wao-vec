odoo.define('wao_pos_cash_move_reason.CashMovePopup', function (require) {
    'use strict';

    const CashMovePopup = require('point_of_sale.CashMovePopup');
    const Registries = require('point_of_sale.Registries');
    const { parse } = require('web.field_utils');
    const { useState } = owl;
    
    
    const CashMovePopupExt = (CashMovePopup) =>
    class extends CashMovePopup {
        // Reason Selection
        // setup() {
        //     super.setup();
        //     this.state = useState({
        //         inputType: '', // '' | 'in' | 'out'
        //         inputAmount: '',
        //         inputReason: '',
        //         inputReasonSelect: '',
        //         inputHasError: false,
        //     });
        // }
        // getPayload() {
        //     return {
        //         amount: parse.float(this.state.inputAmount),
        //         reason: this.state.inputReasonSelect,
        //         type: this.state.inputType,
        //     };
        // }

        // Reason Note Check
        confirm(){
            if (this.state.inputReason.trim().length === 0) {
                this.state.inputHasError = true;
                this.errorMessage = this.env._t('Please set reason note to create cash in / cash out request.');
                return;
            }
            super.confirm();
        }
    }
    Registries.Component.extend(CashMovePopup, CashMovePopupExt);
    return CashMovePopupExt;
});
