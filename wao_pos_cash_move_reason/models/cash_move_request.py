# -*- coding: utf-8 -*-

from odoo import api, fields, models, _


class CashMoveRequest(models.Model):
    _name = 'cash.move.request'
    _description = 'Cash Move Request'

    @api.model
    def _default_company_id(self):
        return self.env.user.company_id

    name = fields.Char(string='Name', default="/", readonly=True, required=True, copy=False)
    pos_session_id = fields.Many2one('pos.session', string='Session', copy=False, required=True)
    currency_id = fields.Many2one('res.currency', string='Currency', related='pos_session_id.currency_id')
    amount = fields.Monetary(string='Amount', required=True, currency_field='currency_id', readonly=True, help='Total amount of the payment.')
    date = fields.Datetime(string='Date', default=fields.Datetime.now)
    reason = fields.Char(string='Reason', required=True)
    company_id = fields.Many2one('res.company', string='Company', default=_default_company_id, required=True)
    state = fields.Selection([
        ('draft', 'Submitted'),
        ('accepted', 'Accepted'),
        ('rejected', 'Rejected'),
    ], required=True, default='draft', string="Status")

    @api.model_create_multi
    def create(self, vals):
        for val in vals:
            if val.get('name', _('/')) == _('/'):
                val['name'] = self.env['ir.sequence'].next_by_code('cash.move.request') or _('/')
        res = super(CashMoveRequest, self).create(vals)
        return res

    def button_accept(self):
        self.env['account.bank.statement.line'].create([
            {
                'pos_session_id': self.pos_session_id.id,
                'journal_id': self.pos_session_id.cash_journal_id.id,
                'amount': self.amount,
                'date': fields.Date.context_today(self),
                'payment_ref': self.reason,
            }
        ])
        message_content = "Accepted request [%s] for the cash move amount : %s." % (self.name, self.amount)
        if self.reason:
            message_content += f'- Reason: {self.reason}'
        self.pos_session_id.message_post(body='<br/>\n' + message_content)

        self.write({'state': 'accepted'})

    def button_reject(self):
        self.write({'state': 'rejected'})
        self.pos_session_id.message_post(body='Request of cash move %s is rejected.' % (self.name))

