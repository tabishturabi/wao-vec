# -*- coding: utf-8 -*-

from odoo import api, fields, models


class PosMoveReason(models.Model):
    _name = "pos.move.reason"
    _description = "PoS - Move In / Out Reason"

    @api.model
    def _default_company_id(self):
        return self.env.user.company_id

    name = fields.Char(required=True)
    active = fields.Boolean(default=True)
    company_id = fields.Many2one('res.company', string='Company', default=_default_company_id, required=True)
