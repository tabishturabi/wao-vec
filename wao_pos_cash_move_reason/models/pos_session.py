# -*- coding: utf-8 -*-

from odoo import fields, models, _
from odoo.exceptions import UserError


class PosSession(models.Model):
    _inherit = "pos.session"

    session_request_count = fields.Integer(compute="_compute_session_request_count")
    request_ids = fields.One2many('cash.move.request', 'pos_session_id')
    failed_pickings = fields.Boolean(compute='_compute_picking_count')
    picking_count = fields.Integer(compute='_compute_picking_count')

    @api.depends('picking_ids', 'picking_ids.state')
    def _compute_picking_count(self):
        for session in self:
            session.picking_count = self.env['stock.picking'].search_count([('pos_session_id', '=', session.id)])
            session.failed_pickings = bool(self.env['stock.picking'].search([('pos_session_id', '=', session.id), ('state', '!=', 'done')], limit=1))

    def _compute_session_request_count(self):
        for rec in self:
            rec.session_request_count = len(rec.request_ids)

    def _pos_ui_models_to_load(self):
        result = super()._pos_ui_models_to_load()
        result.append('pos.move.reason')
        return result

    def _loader_params_pos_move_reason(self):
        return {
            'search_params': {
            'domain': [('active', '=', True)],
            'fields': ['name'],
            },
        }

    def _get_pos_ui_pos_move_reason(self, params):
        return self.env['pos.move.reason'].search_read(**params['search_params'])

    def open_session_requests(self):
        context = dict(self.env.context or {})
        context['default_pos_session_id'] = self.id
        action = {
            'name': _('Cash Move Requests'),
            'view_mode': 'tree,form',
            'res_model': 'cash.move.request',
            'type': 'ir.actions.act_window',
            'domain': [('id', 'in', self.request_ids.ids)],
            'context': context,
            'target': 'current'
        }
        return action

    def try_cash_in_out(self, _type, amount, reason, extras):
        # Override to send notification
        sign = 1 if _type == 'in' else -1
        sessions = self.filtered('cash_journal_id')
        if not sessions:
            raise UserError(_("There is no cash payment method for this PoS Session"))

        request_id = self.env['cash.move.request'].create([
            {
                'pos_session_id': session.id,
                'amount': sign * amount,
                'date': fields.Date.context_today(self),
                'reason': '-'.join([session.name, extras['translatedType'], reason])
            }
            for session in sessions
        ])
        #Send Notification
        user = self.manager_id
        if user:
            template_id = self.env.ref('wao_pos_cash_move_reason.email_template_pos_cash_request', raise_if_not_found=False).id
            template = self.env['mail.template'].browse(template_id)
            for record in self:
                template.send_mail(record.id, force_send=True)
            self.with_context(mail_activity_quick_update=True).sudo().activity_schedule(
            'mail.mail_activity_data_todo', fields.Date.today(),
            summary=_('Approve / Reject POS Session Cash Move Notification - %s.' % (request_id.name,)),
            user_id=user.id)
