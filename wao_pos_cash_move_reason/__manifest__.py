# -*- coding: utf-8 -*-

{
    "name": "POS Cash Move Reason",
    "version": "16.0.0.1",
    "category": "Point of Sale",
    "summary": "POS Cash Move Reason",
    "description": """
        Reason selection when cash in / cash out in the point of sale
        Reason configuration in the backend.
        Approvals on cash in and cash out
        Send notification and email on new cash move request
    """,
    "author": "Tabish",
    "depends": ["point_of_sale", "account", "wao_pos_customizations"],
    "data": [
        # "data/mail_template_data.xml",
        # "data/sequence.xml",
        # "security/ir_rule.xml",
        "security/ir.model.access.csv",
        # "views/view_pos_move_reason.xml",
        # "views/view_pos_session.xml",
        # "views/cash_move_request_view.xml"
    ],
    "assets": {
        "point_of_sale.assets": [
            # "wao_pos_cash_move_reason/static/src/js/models.js",
            # "wao_pos_cash_move_reason/static/src/js/CashMoveButton.js",
            "wao_pos_cash_move_reason/static/src/js/CashMovePopup.js",
            # "wao_pos_cash_move_reason/static/src/scss/pos.scss",
            # "wao_pos_cash_move_reason/static/src/xml/**/*.xml"
        ],

    },
    "auto_install": False,
    "installable": True,
    "license": "OPL-1",
}

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
