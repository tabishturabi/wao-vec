# -*- coding: utf-8 -*-

{
    "name": "POS Customizations",
    "version": "1.0",
    "category": "Point of Sale",
    'summary': 'POS Customizations',
    "description": """
        Force employee to set note when closing balance is not same as opening balance.
        Notify pos manager when closing balance is not matched by email and notification of todo activity.
        Set notes in the journal items for reference.
    """,
    "author": "Tabish",
    "depends": ['point_of_sale', 'account', 'product', 'sale', 'purchase', 'stock','stock_account', 'stock_enterprise'],
    "data": [
        'data/mail_template_data.xml',
        'views/res_settings_config_view.xml',
        'views/account_move_view.xml',
        'views/pos_order_report_view.xml',
        'views/sale_order_pivot_view.xml',
        'views/purchase_report_view.xml',
        'views/stock_views.xml'
    ],
    'assets': {
        'point_of_sale.assets': [
            'wao_pos_customizations/static/src/js/ClosePosPopup.js',
            'wao_pos_customizations/static/src/js/Chrome.js',
            'wao_pos_customizations/static/src/js/TicketScreen.js',
            'wao_pos_customizations/static/src/js/ReceiptScreen.js',
            'wao_pos_customizations/static/src/js/CashMovePopup.js',
            'wao_pos_customizations/static/src/js/CashOpeningPopup.js',
            'wao_pos_customizations/static/src/xml/**/*.xml'
        ],

    },
    "auto_install": False,
    "installable": True,
    "license": "OPL-1",
}

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
