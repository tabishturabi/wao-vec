# -*- coding: utf-8 -*-
# See LICENSE file for full copyright and licensing details.

from odoo import models, _, fields
from odoo.exceptions import UserError


class UpdateSourceWizard(models.TransientModel):
    _name = 'update.source.wizard'
    _description = 'Update Source Wizard'

    source_id = fields.Many2one('stock.location', string='Source')

    def update_source(self):
        picking_ids = self.env.context.get('active_ids', [])
        pickings = self.env['stock.picking'].browse(picking_ids)
        for picking in pickings:
            if picking.state not in ('draft', 'confirmed', 'assigned'):
                raise UserError(_("You can run the Update Wizard when the Transfer status is, Draft, Waiting or Ready."))
            if picking.state == 'assigned':
                picking.do_unreserve()
                if picking.picking_type_code == 'outgoing':
                    picking.write({'location_id': self.source_id.id})
                picking.action_assign()
            elif picking.state == 'confirmed':
                if picking.picking_type_code == 'outgoing':
                    picking.write({'location_id': self.source_id.id})
                    picking.action_assign()
