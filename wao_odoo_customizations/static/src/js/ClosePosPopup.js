odoo.define('wao_pos_customizations.ClosePosPopup', function(require) {
    'use strict';

    const ClosePosPopup = require('point_of_sale.ClosePosPopup');
    const Registries = require('point_of_sale.Registries');
    const { _t } = require('web.core');
    const { renderToString } = require('@web/core/utils/render');
    const { identifyError } = require('point_of_sale.utils');
    const { parse } = require('web.field_utils');
    const { ConnectionLostError, ConnectionAbortedError} = require('@web/core/network/rpc_service');
    const TRANSLATED_CASH_MOVE_TYPE = {
        in: _t('in'),
        out: _t('out'),
    };

    // pass amount to cash out on closing session
    const ClosePosPopupExt = (ClosePosPopup) =>
        class extends ClosePosPopup {
        setup() {
            super.setup();
        }

        openDetailsPopup() {
            super.openDetailsPopup();
            this.state.diff_notes = "";
        }
        handleInputChange(paymentId, event) {
            if (event.target.classList.contains('invalid-cash-input')) return;
            let expectedAmount;
            if (paymentId === this.defaultCashDetails.id) {
                this.manualInputCashCount = true;
                this.state.notes = '';
                this.state.diff_notes = "";
                expectedAmount = this.defaultCashDetails.amount;
            } else {
                expectedAmount = this.otherPaymentMethods.find(pm => paymentId === pm.id).amount;
            }
            this.state.payments[paymentId].counted = parse.float(event.target.value);
            this.state.payments[paymentId].difference =
            this.env.pos.round_decimals_currency(this.state.payments[paymentId].counted - expectedAmount);
        }

        //@override
        //Pass manager closing session and force to enter note
        async confirm() {
            if (!this.cashControl || !this.hasDifference()) {
                this.closeSession();
            } 
            else if (this.hasDifference() && !this.state.diff_notes || this.state.diff_notes.trim().length === 0) {
                await this.showPopup('ConfirmPopup', {
                    title: this.env._t('Payments Difference'),
                    body: _.str.sprintf(
                        this.env._t('Please set closing note to accept the closing difference.')
                    ),
                    confirmText: this.env._t('OK'),
                })
            }
            else if (this.hasUserAuthority()) {
                const { confirmed } = await this.showPopup('ConfirmPopup', {
                    title: this.env._t('Payments Difference'),
                    body: this.env._t('Do you want to accept payments difference and post a profit/loss journal entry?'),
                });
                if (confirmed) {

                    this.closeSession();
                }
            } else {
                await this.showPopup('ConfirmPopup', {
                    title: this.env._t('Payments Difference'),
                    body: _.str.sprintf(
                        this.env._t('The maximum difference allowed is %s.\n\
                        Please contact your manager to accept the closing difference.'),
                        this.env.pos.format_currency(this.amountAuthorizedDiff)
                    ),
                    confirmText: this.env._t('OK'),
                })
            }
        }
        
        // open cash move popup and call closing session
        async closeSession() {

            const paymentLinesObject = this.defaultCashDetails;
            let amountUsedInCashInOut = 0;


            // Get a reference to the CashMovePopup template
            const absDifferences = Object.entries(this.state.payments).map(pm => Math.abs(pm[1].difference))
            let amount_diff = paymentLinesObject.payment_amount - Math.max(...absDifferences)

            if (paymentLinesObject.payment_amount > 0 && amount_diff != 0){
                const { confirmed, payload } = await this.showPopup('CashMovePopup', {
                    amount: amount_diff.toString(),
                    state: {
                      inputAmount: amount_diff.toString(),
                    }
                });

                //get total sales amount and pass it on cashout popup

                if (!confirmed) return;
                const { type, amount, reason } = payload;
                const translatedType = TRANSLATED_CASH_MOVE_TYPE[type];
                const formattedAmount = this.env.pos.format_currency(amount);
                amountUsedInCashInOut = amount;
                if (!amount) {
                    return this.showNotification(
                        _.str.sprintf(this.env._t('Cash in/out of %s is ignored.'), formattedAmount),
                        3000
                    );
                }
                const extras = { formattedAmount, translatedType };


                await this.rpc({
                    model: 'pos.session',
                    method: 'try_cash_in_out',
                    args: [[this.env.pos.pos_session.id], type, amount, reason, extras],
                });
                if (this.env.proxy.printer) {
                    const renderedReceipt = renderToString('point_of_sale.CashMoveReceipt', {
                        _receipt: this._getReceiptInfo({ ...payload, translatedType, formattedAmount }),
                    });
                    const printResult = await this.env.proxy.printer.print_receipt(renderedReceipt);
                    if (!printResult.successful) {
                        this.showPopup('ErrorPopup', { title: printResult.message.title, body: printResult.message.body });
                    }
                }
                this.showNotification(
                    _.str.sprintf(this.env._t('Successfully made a cash %s of %s.'), type, formattedAmount),
                    3000
                );
            }

            if (!this.closeSessionClicked) {
                this.closeSessionClicked = true;
                let response;
                if (this.cashControl) {
                     const countedCash = this.state.payments[this.defaultCashDetails.id].counted - amountUsedInCashInOut;
                     response = await this.rpc({
                        model: 'pos.session',
                        method: 'post_closing_cash_details',
                        args: [this.env.pos.pos_session.id],
                        kwargs: {
                            counted_cash: countedCash,
                        }
                    })
                    if (!response.successful) {
                        return this.handleClosingError(response);
                    }
                }
                await this.rpc({
                    model: 'pos.session',
                    method: 'update_closing_control_state_session',
                    args: [this.env.pos.pos_session.id, this.state.notes, this.state.diff_notes]
                })
                try {
                    const bankPaymentMethodDiffPairs = this.otherPaymentMethods
                        .filter((pm) => pm.type == 'bank')
                        .map((pm) => [pm.id, this.state.payments[pm.id].difference]);
                    response = await this.rpc({
                        model: 'pos.session',
                        method: 'close_session_from_ui',
                        args: [this.env.pos.pos_session.id, bankPaymentMethodDiffPairs],
                        context: this.env.session.user_context,
                    });
                    if (!response.successful) {
                        return this.handleClosingError(response);
                    }
                    window.location = '/web#action=point_of_sale.action_client_pos_menu';
                } catch (error) {
                    const iError = identifyError(error);
                    if (iError instanceof ConnectionLostError || iError instanceof ConnectionAbortedError) {
                        await this.showPopup('ErrorPopup', {
                            title: this.env._t('Network Error'),
                            body: this.env._t('Cannot close the session when offline.'),
                        });
                    } else {
                        await this.showPopup('ErrorPopup', {
                            title: this.env._t('Closing session error'),
                            body: this.env._t(
                                'An error has occurred when trying to close the session.\n' +
                                'You will be redirected to the back-end to manually close the session.')
                        })
                        window.location = '/web#action=point_of_sale.action_client_pos_menu';
                    }
                }
                this.closeSessionClicked = false;
            }
        }
    }

        Registries.Component.extend(ClosePosPopup, ClosePosPopupExt);
        return ClosePosPopupExt;
});
