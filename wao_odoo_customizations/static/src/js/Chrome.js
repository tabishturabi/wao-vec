odoo.define('wao_pos_customizations.Chrome', function(require) {
    'use strict';

    const Chrome = require('point_of_sale.Chrome');
    const Registries = require('point_of_sale.Registries');

    // to hide cash in out button
    const ChromeExt = (Chrome) =>
        class extends Chrome {
        setup() {
            super.setup();
        }

        showCashMoveButton() {
            // return this.mainScreen.name !== 'PaymentScreen';
            return false;
        }
    }
        Registries.Component.extend(Chrome, ChromeExt);
        return ChromeExt;
});
