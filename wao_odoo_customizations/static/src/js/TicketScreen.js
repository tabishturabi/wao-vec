odoo.define('wao_pos_customizations.TicketScreen', function(require) {
    'use strict';

    const TicketScreen = require('point_of_sale.TicketScreen');
    const Registries = require('point_of_sale.Registries');

    const PosTicketScreenExt = TicketScreen => class extends TicketScreen {
        // @Override
        /**
         * @returns {Record<string, { repr: (order: models.Order) => string, displayName: string, modelField: string }>}
         */
        _getSearchFields() {
            const fields = {
                RECEIPT_NUMBER: {
                    repr: (order) => order.name,
                    displayName: this.env._t('Receipt Number'),
                    modelField: 'pos_reference',
                },
                DATE: {
                    repr: (order) => moment(order.creation_date).format('YYYY-MM-DD hh:mm A'),
                    displayName: this.env._t('Date'),
                    modelField: 'date_order',
                },
                PARTNER: {
                    repr: (order) => order.get_partner_name(),
                    displayName: this.env._t('Customer'),
                    modelField: 'partner_id.display_name',
                },
                TOTAL: {
                    repr: (order) =>  this.getTotal(order),
                    displayName: this.env._t('Total'),
                    modelField: 'amount_total',
                },
            };

            if (this.showCardholderName()) {
                fields.CARDHOLDER_NAME = {
                    repr: (order) => order.get_cardholder_name(),
                    displayName: this.env._t('Cardholder Name'),
                    modelField: 'payment_ids.cardholder_name',
                };
            }

            return fields;
        }
    };

    Registries.Component.extend(TicketScreen, PosTicketScreenExt);

    return TicketScreen;
});
