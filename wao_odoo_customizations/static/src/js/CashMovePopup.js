odoo.define('wao_pos_cash_move_reason.CashMovePopupWO', function (require) {
    'use strict';

    const CashMovePopup = require('point_of_sale.CashMovePopup');
    const Registries = require('point_of_sale.Registries');
    const { _t } = require('web.core');
    const { useState } = owl;
    
    // to pass amount from template
    const CashMovePopupExtWO = (CashMovePopup) =>
        class extends CashMovePopup {
            setup() {
                super.setup();
                const { amount, type } = this.props;
                this.state = useState({
                    inputType: type || '', // '' | 'in' | 'out'
                    inputAmount: amount || '',
                    inputReason: '',
                    inputHasError: false,
                });
            }
        };

    CashMovePopupExtWO.defaultProps = {
        cancelText: _t('Cancel'),
        title: _t('Cash In/Out'),
        amount: 0,
        type: '',
    };

    Registries.Component.extend(CashMovePopup, CashMovePopupExtWO);
    return CashMovePopupExtWO;
});
