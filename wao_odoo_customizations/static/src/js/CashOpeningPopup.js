odoo.define('wao_pos_cash_move_reason.CashOpeningPopup', function (require) {
    'use strict';

    const CashOpeningPopup = require('point_of_sale.CashOpeningPopup');
    const Registries = require('point_of_sale.Registries');
    const { _t } = require('web.core');
    const { useState } = owl;

    // to set default opening amount from config
    const CashOpeningPopupExt = (CashOpeningPopup) =>
        class extends CashOpeningPopup {
            setup() {
                super.setup();
                this.manualInputCashCount = null;
                this.state = useState({
                    notes: "",
                    openingCash: this.env.pos.pos_session.cash_register_balance_start || this.env.pos.config.float_amount || 0,
                    displayMoneyDetailsPopup: false,
                });
            }
        };

    CashOpeningPopupExt.defaultProps = {
        cancelText: _t('Cancel'),
        title: _t('Cash In/Out'),
        amount: 0,
        type: '',
    };

    Registries.Component.extend(CashOpeningPopup, CashOpeningPopupExt);
    return CashOpeningPopupExt;
});


