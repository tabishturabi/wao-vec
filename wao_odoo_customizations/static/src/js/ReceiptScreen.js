odoo.define('wao_pos_customizations.ReceiptScreen', function(require) {
    'use strict';

    const ReceiptScreen = require('point_of_sale.ReceiptScreen');
    const Registries = require('point_of_sale.Registries');


    const ReceiptScreenExt = (ReceiptScreen) =>
        class extends ReceiptScreen {
        setup() {
            super.setup();
        }

    async orderDone() {
        super.orderDone();
        this.env.pos.reset_cashier();
        await this.showTempScreen('LoginScreen');
    }
}

    Registries.Component.extend(ReceiptScreen, ReceiptScreenExt);
    return ReceiptScreenExt;
});

