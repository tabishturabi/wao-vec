
from odoo import fields, models
import base64


class BankRecWidget(models.Model):
    _inherit = "bank.rec.widget"

    attachment = fields.Binary(string="Attachment", attachment=True)

    def button_validate(self, async_action=False):
        move = self.st_line_id.move_id
        attachment_data = self.attachment  # Assuming self.attachment contains binary data
        if move and attachment_data:
            move.message_post(
                attachments=[
                    (move.name, base64.decodebytes(attachment_data)),
                ]
            )
        res = super(BankRecWidget, self).button_validate(async_action=False)
        return res

