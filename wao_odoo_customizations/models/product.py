from odoo import api, fields, models, _


class ProductProduct(models.Model):
    _inherit = 'product.product'

    @api.model
    def name_search(self, name='', args=None, operator='ilike', limit=100):
        """Inherit name_search method to search product by product attribute value
        Note: also try with _name_search method, but not getting result as expected"""
        products = super(ProductProduct, self).name_search(name=name, args=args, operator=operator, limit=limit)
        if name:
            product_ids = self.search([('product_template_variant_value_ids.name', operator, name)], limit=limit)
            product_ids = product_ids.name_get()
            product_ids += products
            return product_ids
        return products