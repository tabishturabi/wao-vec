# -*- coding: utf-8 -*-
# See LICENSE file for full copyright and licensing details.

# from . import stock_move
from . import account_move
from . import stock_picking
from . import bank_rec_widget
from . import product
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
