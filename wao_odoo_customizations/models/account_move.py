# -*- coding: utf-8 -*-
# See LICENSE file for full copyright and licensing details.

from odoo import models
from odoo.tools import get_lang


class AccountMove(models.Model):
    _inherit = 'account.move'

    def action_post(self):
        res = super(AccountMove, self).action_post()
        # Send invoice in email with fulfillment details
        if self.move_type != 'entry' and not self.company_id.name in ['Oceania Packaging Company Ltd (NZ)', 'Oceania Packaging Company Pty Ltd (AU)']:
            # template_id = self.env.ref('account.email_template_edi_invoice')
            template_id = self.env['mail.template'].sudo().search([
                ('move_type', '=', 'out_invoice'),
                ('company_id', '=', self.company_id.id),
            ], limit=1)
            lang = False
            if template_id:
                lang = template_id._render_lang(self.ids)[self.id]
            if not lang:
                lang = get_lang(self.env).code
            if not template_id:
                return True
            template = self.env['mail.template'].browse(template_id.id)
            email_ctx = {
                'default_model': 'account.move',
                'default_res_id': self.ids[0],
                'default_use_template': bool(template),
                'default_template_id': template_id.id,
                'default_composition_mode': 'comment',
                'custom_layout': 'mail.mail_notification_layout_with_responsible_signature',
                'force_email': True,
                'mark_invoice_as_sent': True,
                'model_description':self.with_context(lang=lang).type_name,
                'force_email':True,
                'active_id': self.ids[0],
                'active_ids': self.ids,
            }
            template.with_context(**email_ctx).send_mail(self.id, force_send=True)
            if self.sale_order_id and self.sale_order_id.delivery_status in ['full', 'partial']:
                self.sale_order_id.picking_ids.filtered(lambda p: p.state == 'done' and p.picking_type_code == 'outgoing')._send_confirmation_email()
        return res

# class AccountMoveLine(models.Model):
#     _inherit = 'account.move.line'

#     def send_removal_email(self, move_lines_info):
#         # Your email sending logic here
#         subject = "Credit Note Lines Removed"
#         body = "The following lines have been removed from the credit note:<br><br>"
#         # Get the credit note number and ID
#         credit_note_number = self.move_id.name
#         credit_note_id = self.move_id.id

#         # Create the email body
#         body = f"Credit Note Number: {credit_note_number}<br><br>"

#         for info in move_lines_info:
#             body += f"- Product: {info['product']}, Quantity: {info['quantity']}<br>"
        
#         # Get the base URL of your Odoo instance
#         base_url = self.env['ir.config_parameter'].sudo().get_param('web.base.url')

#         # Create the link to the credit note
#         credit_note_link = f"{base_url}/web#id={credit_note_id}&view_type=form&model=account.move"

#         # Add a link to the credit note
#         body += f"<br>Click <a href='{credit_note_link}'>here</a> to view the credit note."


#         # Use self.message_post() to add a note to the credit note
#         self.move_id.message_post(body=body, subject=subject)
#         # Create email
#         email_values = {
#             'subject': subject,
#             'body_html': body,
#             'email_to': self.move_id.invoice_user_id.email or self.env.user.email,  # Assuming partner_id has the email field
#             'model': 'account.move',  # Set the model to the model related to the credit note
#             'res_id': self.move_id.id,  # Set the ID of the credit note
#             'auto_delete': False
#         }

#         # Send the email
#         email = self.env['mail.mail'].create(email_values)
#         email.send()


#     def unlink(self):
#         # Store the relevant information before removing the lines
#         move_lines_info = []
#         for move in self:
#             if move.move_type == 'out_refund':
#                 move_lines_info.append({
#                     'product': move.product_id.name,
#                     'quantity': move.quantity,
#                     # Add more fields as needed
#                 })
#         if move_lines_info:
#             self.send_removal_email(move_lines_info)
#         # Call the unlink method in the super class
#         res = super(AccountMoveLine, self).unlink()
#         return res



# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
