# -*- coding: utf-8 -*-
# See LICENSE file for full copyright and licensing details.

from odoo import models,fields
from odoo.tools import get_lang


class StockPicking(models.Model):
    _inherit = 'stock.picking'


    fulfillment_mail_sent = fields.Boolean(string="Is fulfillment mail sent?")
    is_dispatched_mail_sent = fields.Boolean(string="Is dispatched mail sent?", default=False)

    def print_delivery_receipt(self):
        # Call the print action for the delivery receipt
        return self.env.ref('stock.action_report_delivery').report_action(self)

    def print_related_invoice(self, invoice_ids):
        # Call the print action for the related invoice
        return self.env.ref('account.account_invoices').report_action(invoice_ids)

    def button_validate(self):
        res = super(StockPicking, self).button_validate()
        #Find related invoice and update invoice only if not validated
        for rec in self:
            if rec.picking_type_code == 'incoming' and 'RET' in rec.name and self.sale_id:
                # Get the delivery number and ID
                return_number = self.name

                subject = f"The Return {return_number} for {self.sale_id.name} Has Been Validated."
                body = f"The following products have been validated from the return delivery:<br><br>"
                
                # Create the email body
                body = f"Return Delivery Number: {return_number}<br><br>"
                body += f"Sale Order Number: {self.sale_id.name}<br><br>"
                # for info in self.move_ids_without_package:
                #     body += f"- Product: {info.product_id.name}, Quantity: {info.product_uom_qty} {info.product_uom.name}, Done Quantity: {info.quantity_done} {info.product_uom.name} <br>"
                body += "<table style='border-collapse: collapse; width: 100%; border: 1px solid black;'><tr>"
                body += "<th style='border: 1px solid black;'>Product</th>"
                body += "<th style='border: 1px solid black;'>Reserved Quantity</th>"
                body += "<th style='border: 1px solid black;'>Done Quantity</th>"
                body += "</tr>"

                for info in self.move_ids_without_package:
                    body += f"<tr><td style='border: 1px solid black;'>{info.product_id.name}</td>"
                    body += f"<td style='border: 1px solid black;'>{info.product_uom_qty} {info.product_uom.name}</td>"
                    body += f"<td style='border: 1px solid black;'>{info.quantity_done} {info.product_uom.name}</td></tr>"

                body += "</table>"
                # Get the base URL of your Odoo instance
                base_url = self.env['ir.config_parameter'].sudo().get_param('web.base.url')
                # Create the link to the return delivery
                return_link = f"{base_url}/web#id={self.id}&view_type=form&model=stock.picking"
                # Add a link to the return delivery
                body += f"<br>Click <a href='{return_link}'>here</a> to view the return delivery."

                # Create email
                email_values = {
                    'subject': subject,
                    'body_html': body,
                    'email_to': "operations@waoconnect.com" or self.env.user.email,  # Assuming partner_id has the email field
                    'model': 'stock.picking',  # Set the model to the model related to the return delivery
                    'res_id': self.id,  # Set the ID of the return delivery
                    'auto_delete': False
                }

                # Send the email
                email = self.env['mail.mail'].create(email_values)
                email.send()

            if rec.picking_type_code == 'outgoing':
                # self.print_delivery_receipt()
                sale_id = self.env['sale.order'].search([('name', '=', rec.origin)],limit=1)
                if sale_id and sale_id.invoice_ids:
                    # self.print_related_invoice(sale_id.invoice_ids)

                    invoice_ids = sale_id.invoice_ids.filtered(lambda i: i.move_type == 'out_invoice' and i.state == 'draft')
                    if len(invoice_ids) > 0:
                        move_id = sale_id.invoice_ids.filtered(lambda i: i.move_type == 'out_invoice' and i.state == 'draft')[0]
                        if move_id:
                            scheduled_date = rec.scheduled_date.date()
                            move_id.write({'invoice_date': scheduled_date})
        return res

    def send_invoice_delivery_email(self):
        template_id = self.env.ref('account.email_template_edi_invoice')
        template_id = self.env['mail.template'].sudo().search([
                ('move_type', '=', 'out_invoice'),
                ('company_id', '=', self.company_id.id),
            ], limit=1)
        lang = False
        if template_id:
            lang = template_id._render_lang(self.ids)[self.id]
        if not lang:
            lang = get_lang(self.env).code
        email_ctx = {
            'default_model': 'account.move',
            'default_res_id': self.ids[0],
            'default_use_template': bool(template_id),
            'default_template_id': template_id.id,
            'default_composition_mode': 'comment',
            'custom_layout': 'mail.mail_notification_layout_with_responsible_signature',
            'force_email': True,
            'mark_invoice_as_sent': True,
            'model_description':self.with_context(lang=lang).type_name,
            'force_email':True,
            'active_id': self.ids[0],
            'active_ids': self.ids,
        }
        template_id.with_context(**email_ctx).send_mail(self.id, force_send=True)
        self._send_confirmation_email()

    def write(self, vals):
        res = super(StockPicking, self).write(vals)
        for rec in self:
            if vals.get('state') and vals.get('state') == 'assigned':
                if rec.carrier_id and rec.carrier_id.delivery_type == 'starshipit_vts':
                    vals['fulfillment_mail_sent'] = True
                    rec.send_invoice_delivery_email()
                elif vals.get('carrier_id'):
                    carrier_id = self.env['delivery.carrier'].browse(vals.get('carrier_id'))
                    if carrier_id and carrier_id.delivery_type == 'starshipit_vts':
                        vals['fulfillment_mail_sent'] = True
                        rec.send_invoice_delivery_email()

            elif vals.get('state') and vals.get('state') == 'done' and not rec.fulfillment_mail_sent:
                if rec.carrier_id and rec.carrier_id.delivery_type == 'starshipit_vts':
                    vals['fulfillment_mail_sent'] = True
# <<<<<<< staging
                    self.send_invoice_delivery_email()
            if rec.state == 'done' and 'PICK' in rec.name and not rec.is_dispatched_mail_sent:
                template = self.env['mail.template'].search([('name', '=', 'Order Dispatched')], limit=1)
                template.write({'email_to': rec.partner_id.email})
                template.send_mail(rec.id)
                rec.is_dispatched_mail_sent = True
# =======
#                     rec.send_invoice_delivery_email()
#                 elif vals.get('carrier_id'):
#                     carrier_id = self.env['delivery.carrier'].browse(vals.get('carrier_id'))
#                     if carrier_id and carrier_id.delivery_type == 'starshipit_vts':
#                         vals['fulfillment_mail_sent'] = True
#                         rec.send_invoice_delivery_email()
#             if rec.state == 'done' and 'OUT' in rec.name and not rec.is_dispatched_mail_sent:
#                 template = self.env['mail.template'].search([('name', '=', 'Order Dispatched')], limit=1)
#                 template.write({'email_to': rec.partner_id.email})
#                 template.send_mail(rec.id)
#                 rec.is_dispatched_mail_sent = True
# >>>>>>> uat
        return res


# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
